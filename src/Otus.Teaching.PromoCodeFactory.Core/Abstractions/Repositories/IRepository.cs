﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] navigationProperties);

        Task<IEnumerable<proj_T>> GetAllAsync<proj_T>(params Expression<Func<T, object>>[] navigationProperties);

        Task<T> GetByIdAsync(Guid id,
            params Expression<Func<T, object>>[] navigationProperties);

        Task<proj_T> GetByIdAsync<proj_T>(Guid id,
            params Expression<Func<T, object>>[] navigationProperties);

        Task<IEnumerable<T>> GetByFilterAsync(Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] navigationProperties);

        Task<IEnumerable<proj_T>> GetByFilterAsync<proj_T>(Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] navigationProperties);

        Task<bool> AnyAsync(Guid id);

        Task<bool> AnyAsync(Expression<Func<T, bool>> predicate);

        Task<T> GetByIdToUpdateAsync(Guid id,
            params Expression<Func<T, object>>[] navigationProperties);

        EntityEntry<T> Create(T entity);

        void CreateRange(IEnumerable<T> entities);

        EntityEntry<T> Update(T entity);

        void UpdateRange(IEnumerable<T> entities);

        EntityEntry<T> Delete(T entity);

        void DeleteRange(IEnumerable<T> entities);
    }
}