﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        [Column("name", TypeName = "NVARCHAR(32)")]
        public string Name { get; set; }

        [Column("description", TypeName = "NVARCHAR(256)")]
        public string Description { get; set; }
    }
}