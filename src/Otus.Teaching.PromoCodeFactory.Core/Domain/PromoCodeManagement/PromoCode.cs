﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        [Column("code", TypeName = "NVARCHAR(32)")]
        public string Code { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Column("service_info", TypeName = "NVARCHAR(128)")]
        public string ServiceInfo { get; set; }

        [Column("begin_date")]
        public DateTime BeginDate { get; set; }

        [Column("end_date")]
        public DateTime EndDate { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Column("partner_name", TypeName = "NVARCHAR(64)")]
        public string PartnerName { get; set; }

        [Column("partner_manager_id")]
        public Guid PartnerManagerId { get; set; }

        [Column("preference_id")]
        public Guid PreferenceId { get; set; }

        [Column("customer_id")]
        public Guid CustomerId { get; set; }

        public virtual Employee PartnerManager { get; set; }

        public virtual Preference Preference { get; set; }

        public virtual Customer Customer { get; set; }
    }
}