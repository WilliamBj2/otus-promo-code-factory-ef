﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        [Column("name", TypeName = "NVARCHAR(32)")]
        public string Name { get; set; }
    }
}