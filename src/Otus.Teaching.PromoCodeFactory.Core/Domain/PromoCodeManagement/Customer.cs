﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        [Column("first_name", TypeName = "NVARCHAR(32)")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Column("last_name", TypeName = "NVARCHAR(32)")]
        public string LastName { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Column("email", TypeName = "NVARCHAR(64)")]
        public string Email { get; set; }

        public virtual HashSet<CustomerPreference> CustomerPreferences { get; set; }

        public virtual HashSet<PromoCode> PromoCodes { get; set; }

        [NotMapped]
        public string FullName => $"{FirstName} {LastName}";
    }
}