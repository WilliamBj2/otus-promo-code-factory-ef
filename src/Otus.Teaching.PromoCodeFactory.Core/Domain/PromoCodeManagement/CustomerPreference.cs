﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference
        : BaseEntity
    {
        [Column("customer_id")]
        public Guid CustomerId { get; set; }

        [Column("preference_id")]
        public Guid PreferenceId { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual Preference Preference { get; set; }
    }
}
