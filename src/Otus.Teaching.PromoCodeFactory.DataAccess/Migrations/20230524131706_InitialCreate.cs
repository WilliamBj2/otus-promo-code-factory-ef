﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    first_name = table.Column<string>(type: "NVARCHAR(32)", nullable: false),
                    last_name = table.Column<string>(type: "NVARCHAR(32)", nullable: false),
                    email = table.Column<string>(type: "NVARCHAR(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Preferences",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    name = table.Column<string>(type: "NVARCHAR(32)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Preferences", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    name = table.Column<string>(type: "NVARCHAR(32)", nullable: false),
                    description = table.Column<string>(type: "NVARCHAR(128)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerPreferences",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    customer_id = table.Column<Guid>(type: "TEXT", nullable: false),
                    preference_id = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerPreferences", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerPreferences_Customers_customer_id",
                        column: x => x.customer_id,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerPreferences_Preferences_preference_id",
                        column: x => x.preference_id,
                        principalTable: "Preferences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    first_name = table.Column<string>(type: "NVARCHAR(32)", nullable: false),
                    last_name = table.Column<string>(type: "NVARCHAR(32)", nullable: false),
                    email = table.Column<string>(type: "NVARCHAR(64)", nullable: false),
                    role_id = table.Column<Guid>(type: "TEXT", nullable: false),
                    applied_promocodes_count = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employees_Roles_role_id",
                        column: x => x.role_id,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PromoCodes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    code = table.Column<string>(type: "NVARCHAR(32)", nullable: false),
                    service_info = table.Column<string>(type: "NVARCHAR(128)", nullable: false),
                    begin_date = table.Column<DateTime>(type: "TEXT", nullable: false),
                    end_date = table.Column<DateTime>(type: "TEXT", nullable: false),
                    partner_name = table.Column<string>(type: "NVARCHAR(64)", nullable: false),
                    partner_manager_id = table.Column<Guid>(type: "TEXT", nullable: false),
                    preference_id = table.Column<Guid>(type: "TEXT", nullable: false),
                    customer_id = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromoCodes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PromoCodes_Customers_customer_id",
                        column: x => x.customer_id,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PromoCodes_Employees_partner_manager_id",
                        column: x => x.partner_manager_id,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PromoCodes_Preferences_preference_id",
                        column: x => x.preference_id,
                        principalTable: "Preferences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "email", "first_name", "last_name" },
                values: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), "ivan_sergeev@mail.ru", "Иван", "Петров" });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "name" },
                values: new object[] { new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), "Театр" });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "name" },
                values: new object[] { new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"), "Семья" });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "name" },
                values: new object[] { new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), "Дети" });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "name" },
                values: new object[] { new Guid("e566714d-d375-4247-9bf2-1a67b4f8eebe"), "Путешествия" });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "name" },
                values: new object[] { new Guid("4dbb26fe-c1c7-4ae1-85d1-65daf0928719"), "Иностранные языки" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "description", "name" },
                values: new object[] { new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "Администратор", "Admin" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "description", "name" },
                values: new object[] { new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665"), "Партнерский менеджер", "PartnerManager" });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "customer_id", "preference_id" },
                values: new object[] { new Guid("5838a34b-5ab6-4216-982a-545fe1816240"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "customer_id", "preference_id" },
                values: new object[] { new Guid("fa31ad29-e391-474d-a5ce-b8e35bb4b9d7"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("e566714d-d375-4247-9bf2-1a67b4f8eebe") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "customer_id", "preference_id" },
                values: new object[] { new Guid("3c4ece6d-83d7-45e9-ac99-a037ddaca57b"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("4dbb26fe-c1c7-4ae1-85d1-65daf0928719") });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "applied_promocodes_count", "email", "first_name", "last_name", "role_id" },
                values: new object[] { new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), 5, "owner@somemail.ru", "Иван", "Сергеев", new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02") });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "applied_promocodes_count", "email", "first_name", "last_name", "role_id" },
                values: new object[] { new Guid("f766e2bf-340a-46ea-bff3-f1700b435895"), 10, "andreev@somemail.ru", "Петр", "Андреев", new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665") });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPreferences_customer_id",
                table: "CustomerPreferences",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPreferences_preference_id",
                table: "CustomerPreferences",
                column: "preference_id");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_role_id",
                table: "Employees",
                column: "role_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PromoCodes_customer_id",
                table: "PromoCodes",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_PromoCodes_partner_manager_id",
                table: "PromoCodes",
                column: "partner_manager_id");

            migrationBuilder.CreateIndex(
                name: "IX_PromoCodes_preference_id",
                table: "PromoCodes",
                column: "preference_id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerPreferences");

            migrationBuilder.DropTable(
                name: "PromoCodes");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Preferences");

            migrationBuilder.DropTable(
                name: "Roles");
        }
    }
}
