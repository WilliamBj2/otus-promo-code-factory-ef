﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class UpdateDescriptionField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("3c4ece6d-83d7-45e9-ac99-a037ddaca57b"));

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("5838a34b-5ab6-4216-982a-545fe1816240"));

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("fa31ad29-e391-474d-a5ce-b8e35bb4b9d7"));

            migrationBuilder.AlterColumn<string>(
                name: "description",
                table: "Roles",
                type: "NVARCHAR(256)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "NVARCHAR(128)",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "customer_id", "preference_id" },
                values: new object[] { new Guid("4041e70b-0cab-4e89-9d5f-1d2bed873568"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "customer_id", "preference_id" },
                values: new object[] { new Guid("145ece39-3a5d-42ba-af5a-9fba53874b56"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("e566714d-d375-4247-9bf2-1a67b4f8eebe") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "customer_id", "preference_id" },
                values: new object[] { new Guid("13656e3b-2d36-4e85-8d08-68094e476d03"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("4dbb26fe-c1c7-4ae1-85d1-65daf0928719") });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("13656e3b-2d36-4e85-8d08-68094e476d03"));

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("145ece39-3a5d-42ba-af5a-9fba53874b56"));

            migrationBuilder.DeleteData(
                table: "CustomerPreferences",
                keyColumn: "Id",
                keyValue: new Guid("4041e70b-0cab-4e89-9d5f-1d2bed873568"));

            migrationBuilder.AlterColumn<string>(
                name: "description",
                table: "Roles",
                type: "NVARCHAR(128)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "NVARCHAR(256)",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "customer_id", "preference_id" },
                values: new object[] { new Guid("5838a34b-5ab6-4216-982a-545fe1816240"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "customer_id", "preference_id" },
                values: new object[] { new Guid("fa31ad29-e391-474d-a5ce-b8e35bb4b9d7"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("e566714d-d375-4247-9bf2-1a67b4f8eebe") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "customer_id", "preference_id" },
                values: new object[] { new Guid("3c4ece6d-83d7-45e9-ac99-a037ddaca57b"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("4dbb26fe-c1c7-4ae1-85d1-65daf0928719") });
        }
    }
}
