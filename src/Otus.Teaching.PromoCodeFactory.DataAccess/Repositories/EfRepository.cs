﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using AutoMapper;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<T> _dbSet;
        private readonly IMapper _mapper;

        public EfRepository(DataContext context, IMapper mapper)
        {
            _dbContext = context;
            _dbSet = context.Set<T>();
            _mapper = mapper;
        }

        private IQueryable<T> Include(IQueryable<T> source, Expression<Func<T, object>>[] navigationProperties) =>
            navigationProperties is null
            ? source
            : navigationProperties.Aggregate(source, (current, navigationProperty) => current.Include(navigationProperty));

        public async Task<IEnumerable<T>>
            GetAllAsync(params Expression<Func<T, object>>[] navigationProperties) =>
            await Include(_dbSet.AsNoTrackingWithIdentityResolution(), navigationProperties).ToListAsync();

        public async Task<IEnumerable<proj_T>>
            GetAllAsync<proj_T>(params Expression<Func<T, object>>[] navigationProperties) =>
            await Include(_dbSet.AsNoTrackingWithIdentityResolution(), navigationProperties)
                .ProjectTo<proj_T>(_mapper.ConfigurationProvider).ToListAsync();

        public async Task<T> GetByIdAsync(Guid id,
            params Expression<Func<T, object>>[] navigationProperties) =>
            await Include(_dbSet.AsNoTrackingWithIdentityResolution(), navigationProperties)
                .Where(x => x.Id == id).SingleOrDefaultAsync();

        public async Task<proj_T> GetByIdAsync<proj_T>(Guid id,
            params Expression<Func<T, object>>[] navigationProperties) =>
            await Include(_dbSet.AsNoTrackingWithIdentityResolution(), navigationProperties)
                .Where(x => x.Id == id).ProjectTo<proj_T>(_mapper.ConfigurationProvider).SingleOrDefaultAsync();

        public async Task<IEnumerable<T>> GetByFilterAsync(Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] navigationProperties) =>
            await Include(_dbSet.AsNoTrackingWithIdentityResolution(), navigationProperties)
                .Where(predicate).ToListAsync();

        public async Task<IEnumerable<proj_T>> GetByFilterAsync<proj_T>(Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] navigationProperties) =>
            await Include(_dbSet.AsNoTrackingWithIdentityResolution(), navigationProperties)
                .Where(predicate).ProjectTo<proj_T>(_mapper.ConfigurationProvider).ToListAsync();

        public async Task<bool> AnyAsync(Guid id) =>
            await _dbSet.AsNoTracking().AnyAsync(x => x.Id == id);

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> predicate) =>
            await _dbSet.AsNoTracking().AnyAsync(predicate);

        public async Task<T> GetByIdToUpdateAsync(Guid id,
            params Expression<Func<T, object>>[] navigationProperties) =>
            await Include(_dbSet, navigationProperties)
                .Where(x => x.Id == id).SingleOrDefaultAsync();

        public EntityEntry<T> Create(T entity) => _dbSet.Add(entity);

        public void CreateRange(IEnumerable<T> entities) => _dbSet.AddRange(entities);

        public EntityEntry<T> Update(T entity) => _dbSet.Update(entity);

        public void UpdateRange(IEnumerable<T> entities) => _dbSet.AddRange(entities);

        public EntityEntry<T> Delete(T entity) => _dbSet.Remove(entity);

        public void DeleteRange(IEnumerable<T> entities) => _dbSet.RemoveRange(entities);
    }
}
