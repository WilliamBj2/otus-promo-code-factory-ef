﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Managers;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        IUnitOfWork _unitOfWork;
        IMapper _mapper;

        public CustomersController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<ICollection<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _unitOfWork
                .GetRepository<Customer>()
                .GetAllAsync<CustomerShortResponse>();

            return Ok(customers);
        }

        /// <summary>
        /// Получить информацию по клиенту
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _unitOfWork
                .GetRepository<Customer>()
                .GetByIdAsync<CustomerResponse>(id, x => x.PromoCodes);

            var customerPreferences = await _unitOfWork
                .GetRepository<CustomerPreference>()
                .GetByFilterAsync(x => x.CustomerId == id, x => x.Preference);

            customer.Preferences = customerPreferences
                .Select(x => _mapper.Map<PreferenceResponse>(x.Preference));

            return Ok(customer);
        }

        /// <summary>
        /// Регистрация нового клиента
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync([FromBody] CreateOrEditCustomerRequest request)
        {
            var customer = _mapper.Map<Customer>(request);
            var preferences = await _unitOfWork
                .GetRepository<Preference>()
                .GetByFilterAsync(x => request.PreferenceIds.Contains(x.Id));

            if (preferences.Count() == request.PreferenceIds.Count())
            {
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    _unitOfWork.GetRepository<CustomerPreference>().CreateRange(
                        from p in preferences
                        select new CustomerPreference
                        {
                            Customer = customer,
                            PreferenceId = p.Id
                        });

                    await _unitOfWork.SaveChangesAsync();
                    await transaction.CommitAsync();

                    return Ok(customer.Id);
                }
                catch
                {
                    await transaction.RollbackAsync();
                    return Forbid("Ошибка в процессе выполнения транзакции.");
                }
            }
            else
            {
                return BadRequest("Указаны отсутствующие идентификаторы Preference.");
            }
        }

        /// <summary>
        /// Обновление данных о клиенте
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, [FromBody] CreateOrEditCustomerRequest request)
        {
            var customer = await _unitOfWork.GetRepository<Customer>().GetByIdToUpdateAsync(id, x => x.CustomerPreferences);
            if (customer is null)
            {
                return BadRequest("Указан отсутствующий идентификатор Customer.");
            }
            else
            {
                var preferences = await _unitOfWork
                    .GetRepository<Preference>()
                    .GetByFilterAsync(x => request.PreferenceIds.Contains(x.Id));

                if (preferences.Count() == request.PreferenceIds.Count())
                {
                    var transaction = await _unitOfWork.BeginTransactionAsync();
                    try
                    {
                        customer.FirstName = request.FirstName;
                        customer.LastName = request.LastName;
                        customer.Email = request.Email;

                        _unitOfWork.GetRepository<CustomerPreference>().DeleteRange(customer.CustomerPreferences);

                        _unitOfWork.GetRepository<CustomerPreference>().CreateRange(
                            from p in preferences
                            select new CustomerPreference
                            {
                                CustomerId = customer.Id,
                                PreferenceId = p.Id
                            });

                        await _unitOfWork.SaveChangesAsync();
                        await transaction.CommitAsync();

                        return Ok();
                    }
                    catch
                    {
                        await transaction.RollbackAsync();
                        return Forbid("Ошибка в процессе выполнения транзакции.");
                    }
                }
                else
                {
                    return BadRequest("Указаны отсутствующие идентификаторы Preference.");
                }
            }
        }

        /// <summary>
        /// Удаление данных о клиенте
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _unitOfWork.GetRepository<Customer>().GetByIdAsync(id, x => x.PromoCodes, x => x.CustomerPreferences);
            if (customer == null)
            {
                return BadRequest("Указан отсутствующий идентификатор Customer.");
            }
            else
            {
                var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    _unitOfWork.GetRepository<Customer>().Delete(customer);

                    await _unitOfWork.SaveChangesAsync();
                    await transaction.CommitAsync();

                    return Ok();
                }
                catch
                {
                    await transaction.RollbackAsync();
                    return Forbid("Ошибка в процессе выполнения транзакции.");
                }
            }
        }
    }
}