﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Managers;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения (справочник)
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController
        : ControllerBase
    {
        IUnitOfWork _unitOfWork;

        public PreferenceController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<ICollection<PreferenceResponse>>> GetPreferences()
        {
            var preferences = await _unitOfWork
                .GetRepository<Preference>()
                .GetAllAsync<PreferenceResponse>();

            return Ok(preferences);
        }

        /// <summary>
        /// Получить запись по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<PreferenceResponse>> GetPreference(Guid id)
        {
            var preference = await _unitOfWork
                .GetRepository<Preference>()
                .GetByIdAsync<PreferenceResponse>(id);

            return Ok(preference);
        }
    }
}
