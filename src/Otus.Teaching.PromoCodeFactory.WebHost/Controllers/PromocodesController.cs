﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Managers;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public PromocodesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _unitOfWork
                .GetRepository<PromoCode>()
                .GetAllAsync<PromoCodeShortResponse>();

            return Ok(promocodes);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var employee = (await _unitOfWork
                .GetRepository<Employee>()
                .GetByFilterAsync(x => x.FirstName == request.PartnerName)).ElementAt(0);

            if (employee == null)
                return BadRequest("Отсутствуют Employee, соответствующие указанному условию.");

            var customerPreferences = await _unitOfWork
                .GetRepository<CustomerPreference>()
                .GetByFilterAsync(x => x.Preference.Name == request.Preference);

            if (customerPreferences.Count() == 0)
                return BadRequest("Отсутствуют Customer, соответствующие указанному условию.");

            _unitOfWork.GetRepository<PromoCode>().CreateRange(
                from cp in customerPreferences
                select new PromoCode
                {
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    BeginDate = DateTime.Now,
                    EndDate = new DateTime(2222, 1, 1),
                    PartnerName = employee.FirstName,
                    PartnerManagerId = employee.Id,
                    PreferenceId = cp.PreferenceId,
                    CustomerId = cp.CustomerId
                });

            await _unitOfWork.SaveChangesAsync();
            return Ok();
        }
    }
}