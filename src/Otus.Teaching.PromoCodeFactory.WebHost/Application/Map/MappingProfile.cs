﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Application.Map
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Customer, CustomerShortResponse>();

            CreateMap<Customer, CustomerResponse>();
            CreateMap<Preference, PreferenceResponse>();
            CreateMap<PromoCode, PromoCodeShortResponse>()
                .ForMember(x => x.BeginDate, opt => opt.MapFrom(s => s.BeginDate.ToString()))
                .ForMember(x => x.EndDate, opt => opt.MapFrom(s => s.EndDate.ToString()));

            CreateMap<CreateOrEditCustomerRequest, Customer>();
        }
    }
}
